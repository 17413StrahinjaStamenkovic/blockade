import os
import re
import timeit
import config
import copy
import json
from zobrist import Zobrist


transposition_table = {}
possible_wall_states = set()


def save_transposition_table(table, filename):
    dump = json.dumps(table)
    f = open(filename, "w")
    f.write(dump)
    f.close()


def load_transposition_table(filename):
    with open(filename) as tp:
        data = json.load(tp)
        return dict(zip(map(int, data), data.values()))


class Blockade:
    __slots__ = (
        'is_pve',
        'player_symbol',
        'ai_symbol',
        'is_pve',
        'player_symbol',
        'ai_symbol',

        'm', 'n',
        'k',
        'v_walls',
        'h_walls',

        'turn_to_play',
        'player_x',
        'player_o',

        'possible_v_walls',
        'possible_h_walls',

        'zobrist',
        'zobrist_hash'
    )

    def __init__(self) -> None:
        self.is_pve = None
        self.player_symbol = None
        self.ai_symbol = None

        self.m, self.n = None, None
        self.k = None

        self.v_walls = None
        self.h_walls = None

        self.turn_to_play = None
        self.player_x = None
        self.player_o = None

        self.possible_v_walls = None
        self.possible_h_walls = None

        self.zobrist = None
        self.zobrist_hash = None

    def initialize(self):
        self.is_pve = self.select_game_mode()
        self.player_symbol = None
        self.ai_symbol = None
        if(self.is_pve):
            self.player_symbol = self.select_symbol()
            self.ai_symbol = "O" if self.player_symbol == "X" else "X"

        self.m, self.n = self.input_dimensions()
        self.k = self.input_number_of_walls()

        self.v_walls = set()
        self.h_walls = set()

        self.turn_to_play = 'X'

        self.player_x = {
            "symbol": "X",
            # 2 polja u (i,j) formatu
            "start_pos": self.input_starting_positions("X"),
            "v_walls_available": self.k,
            "h_walls_available": self.k,
        }
        self.player_x['pawns'] = {
            # current indices of pawns 1 and 2
            1: self.player_x["start_pos"][0],
            2: self.player_x["start_pos"][1]
        }
        self.player_o = {
            "symbol": "O",
            # 2 polja u (i,j) formatu
            "start_pos": self.input_starting_positions("O"),
            "v_walls_available": self.k,
            "h_walls_available": self.k,
        }
        self.player_o['pawns'] = {
            # current indices of pawns 1 and 2
            1: self.player_o["start_pos"][0],
            2: self.player_o["start_pos"][1]
        }

        self.possible_v_walls = self.generate_wall_placements()
        self.possible_h_walls = {*self.possible_v_walls}

        self.zobrist = Zobrist()
        self.zobrist_hash = self.zobrist.calculate_hash(self)

    def initialize_default(self):
        self.is_pve = config.is_pve
        self.player_symbol = config.player_symbol
        self.ai_symbol = config.ai_symbol

        self.m, self.n = config.m, config.n
        self.k = config.k

        self.v_walls = set()
        self.h_walls = set()

        self.turn_to_play = 'X'

        self.player_x = {
            "symbol": "X",
            # 2 polja u (i,j) formatu
            "start_pos": config.player_x_start_pos,
            "v_walls_available": config.k,
            "h_walls_available": config.k,
            "pawns": {
                # current indices of pawns 1 and 2
                1: config.player_x_start_pos[0],
                2: config.player_x_start_pos[1]
            }
        }
        self.player_o = {
            "symbol": "O",
            # 2 polja u (i,j) formatu
            "start_pos": config.player_o_start_pos,
            "v_walls_available": config.k,
            "h_walls_available": config.k,
            "pawns": {
                # current indices of pawns 1 and 2
                1: config.player_o_start_pos[0],
                2: config.player_o_start_pos[1]
            }
        }
        self.possible_v_walls = self.generate_wall_placements()
        self.possible_h_walls = {*self.possible_v_walls}

        self.zobrist = Zobrist()
        self.zobrist_hash = self.zobrist.calculate_hash(self)
        self.zobrist.save_to_file()

    def __copy__(self):
        new_state = Blockade()

        new_state.is_pve = self.is_pve
        new_state.player_symbol = self.player_symbol
        new_state.ai_symbol = self.ai_symbol

        new_state.m, new_state.n = self.m, self.n
        new_state.k = self.k

        new_state.v_walls = {*self.v_walls}
        new_state.h_walls = {*self.h_walls}

        new_state.turn_to_play = self.turn_to_play

        new_state.player_x = {**self.player_x}
        new_state.player_x['pawns'] = {**self.player_x['pawns']}

        new_state.player_o = {**self.player_o}
        new_state.player_o['pawns'] = {**self.player_o['pawns']}

        new_state.possible_v_walls = {*self.possible_v_walls}
        new_state.possible_h_walls = {*self.possible_h_walls}

        new_state.zobrist = self.zobrist
        new_state.zobrist_hash = self.zobrist_hash
        return new_state

    def get_neighbours(self, player_symbol, field, pathfinding=False):
        (i, j) = field
        (player, opponent) = (self.player_x, self.player_o) if player_symbol == 'X' else (
            self.player_o, self.player_x)
        player_pawns = {*player['pawns'].values()}
        opponent_pawns = {*opponent['pawns'].values()}
        opponent_starts = {*opponent['start_pos']}
        taken_fields = opponent_pawns if pathfinding else {
            *player_pawns, *opponent_pawns}
        taken_non_start_fields = taken_fields.difference(opponent_starts)

        neighbours = set()
        # region diagonal
        if (self.within_bounds(i-1, j-1) and
            not (self.is_field_blocked((i-1, j-1), bottom=True, right=True) or self.is_field_blocked((i, j), top=True, left=True)) and
            not (self.is_field_blocked((i-1, j-1), right=True) and self.is_field_blocked((i, j), left=True)) and
            not (self.is_field_blocked((i-1, j-1), bottom=True) and self.is_field_blocked((i, j), top=True)) and
            not (i-1, j-1) in taken_non_start_fields
            ):
            neighbours.add((i-1, j-1))

        if (self.within_bounds(i-1, j+1) and
            not (self.is_field_blocked((i-1, j+1), bottom=True, left=True) or self.is_field_blocked((i, j), top=True, right=True)) and
            not (self.is_field_blocked((i-1, j+1), left=True) and self.is_field_blocked((i, j), right=True)) and
            not (self.is_field_blocked((i-1, j+1), bottom=True) and self.is_field_blocked((i, j), top=True)) and
                not (i-1, j+1) in taken_non_start_fields
            ):
            neighbours.add((i-1, j+1))

        if (self.within_bounds(i+1, j-1) and
            not (self.is_field_blocked((i+1, j-1), top=True, right=True) or self.is_field_blocked((i, j), bottom=True, left=True)) and
            not (self.is_field_blocked((i+1, j-1), right=True) and self.is_field_blocked((i, j), left=True)) and
            not (self.is_field_blocked((i+1, j-1), top=True) and self.is_field_blocked((i, j), bottom=True)) and
                not (i+1, j-1) in taken_non_start_fields
            ):
            neighbours.add((i+1, j-1))

        if (self.within_bounds(i+1, j+1) and
            not (self.is_field_blocked((i+1, j+1), top=True, left=True) or self.is_field_blocked((i, j), bottom=True, right=True)) and
            not (self.is_field_blocked((i+1, j+1), left=True) and self.is_field_blocked((i, j), right=True)) and
            not (self.is_field_blocked((i+1, j+1), top=True) and self.is_field_blocked((i, j), bottom=True)) and
                not (i+1, j+1) in taken_non_start_fields
            ):
            neighbours.add((i+1, j+1))
        # endregion

        # region horizontal and vertical movement
        if self.within_bounds(i+1, j) and not self.is_field_blocked((i+1, j), top=True):
            if (i+1, j) in opponent_starts or pathfinding and (i+1, j) in player_pawns:
                neighbours.add((i+1, j))
            if self.within_bounds(i+2, j) and not self.is_field_blocked((i+1, j), bottom=True):
                if (i+2, j) in opponent_pawns:
                    neighbours.add((i+1, j))
                    if (i+2, j) in opponent_starts:
                        neighbours.add((i+2, j))
                elif (i+2, j) not in player_pawns or pathfinding:
                    neighbours.add((i+2, j))

        if self.within_bounds(i-1, j) and not self.is_field_blocked((i-1, j), bottom=True):
            if (i-1, j) in opponent_starts or pathfinding and (i-1, j) in player_pawns:
                neighbours.add((i-1, j))
            if self.within_bounds(i-2, j) and not self.is_field_blocked((i-1, j), top=True):
                if (i-2, j) in opponent_pawns:
                    neighbours.add((i-1, j))
                    if (i-2, j) in opponent_starts:
                        neighbours.add((i-2, j))
                elif (i-2, j) not in player_pawns or pathfinding:
                    neighbours.add((i-2, j))

        if self.within_bounds(i, j+1) and not self.is_field_blocked((i, j+1), left=True):
            if (i, j+1) in opponent_starts or pathfinding and (i, j+1) in player_pawns:
                neighbours.add((i, j+1))
            if self.within_bounds(i, j+2) and not self.is_field_blocked((i, j+1), right=True):
                if (i, j+2) in opponent_pawns:
                    neighbours.add((i, j+1))
                    if (i, j+2) in opponent_starts:
                        neighbours.add((i, j+2))
                elif (i, j+2) not in player_pawns or pathfinding or (i, j+2) in opponent_starts:
                    neighbours.add((i, j+2))

        if self.within_bounds(i, j-1) and not self.is_field_blocked((i, j-1), right=True):
            if (i, j-1) in opponent_starts or pathfinding and (i, j-1) in player_pawns:
                neighbours.add((i, j-1))
            if self.within_bounds(i, j-2) and not self.is_field_blocked((i, j-1), left=True):
                if (i, j-2) in opponent_pawns:
                    neighbours.add((i, j-1))
                    if (i, j-2) in opponent_starts:
                        neighbours.add((i, j-2))
                elif (i, j-2) not in player_pawns or pathfinding:
                    neighbours.add((i, j-2))
        # endregion
        return neighbours

    def within_bounds(self, i, j):
        return 1 <= i <= self.m and 1 <= j <= self.n

    def update_possible_walls_on_placement(self, wall_pos, is_vertical):
        (i, j) = wall_pos
        if is_vertical:
            self.possible_v_walls.discard((i-1, j))
            self.possible_v_walls.discard((i, j))
            self.possible_v_walls.discard((i+1, j))
            self.possible_h_walls.discard((i, j))
        else:
            self.possible_h_walls.discard((i, j-1))
            self.possible_h_walls.discard((i, j))
            self.possible_h_walls.discard((i, j+1))
            self.possible_v_walls.discard((i, j))

    def is_wall_connected(self, field, is_vertical):
        (i, j) = field
        counter = 1 if (is_vertical and (i == 1 or i == self.m-1)
                        ) or (not is_vertical and (j == 1 or j == self.n-1)) else 0
        v_connections = {(i-2, j), (i+2, j)}
        h_connections = {(i-1, j-1), (i-1, j), (i-1, j+1),
                         (i, j-1), (i, j+1),
                         (i+1, j-1), (i+1, j), (i+1, j+1)}
        if not is_vertical:
            v_connections = h_connections
            h_connections = {(i, j-2), (i, j+2)}
        counter += len(self.v_walls.intersection(v_connections))
        counter += len(self.h_walls.intersection(h_connections))

        return counter > 1

    def int_to_field_num(self, number: int):
        return str(number) if (number < 10) else chr(65-10+number)

    def field_num_to_int(self, field: str):
        return int(field) if ('1' <= field <= '9') else (ord(field.upper())-ord('A')+10)

    def is_finished(self):
        return (self.player_x['pawns'][1] in self.player_o['start_pos'] or
                self.player_x['pawns'][2] in self.player_o['start_pos'] or
                self.player_o['pawns'][1] in self.player_x['start_pos'] or
                self.player_o['pawns'][2] in self.player_x['start_pos']
                )

    # region Display methods
    def print_table(self):
        # enumeracija kolona
        table = []

        col_enumeration = " "+"".join(
            f''' {self.int_to_field_num(j).upper()}''' for j in range(1, self.n+1))
        table.append(col_enumeration)
        table.append(" "+" ="*self.n)

        for i in range(1, self.m+1):
            row_enumeration = self.int_to_field_num(i)+"ǁ"
            row = row_enumeration
            bottom_wall = ' '
            for j in range(1, self.n+1):
                symbol = "X" if (i, j) in self.player_x['pawns'].values() else "O" if (
                    i, j) in self.player_o['pawns'].values() else " "

                right_wall = "" if(j == self.n) else "ǁ" if (
                    (i, j) in self.v_walls or (i-1, j) in self.v_walls) else "|"

                row += symbol + right_wall

                if(i != self.m):
                    bottom_wall += " =" if ((i, j) in self.h_walls or (i,
                                            j-1) in self.h_walls) else " -"
                else:
                    bottom_wall = " " + (" ="*self.n)
            row += row_enumeration[::-1]
            table.append(row)
            table.append(bottom_wall)
        table.append(col_enumeration)

        for row in table:
            print(row)

    def print_positions(self):
        print(f'''Current positions: ''')
        print(
            f'''\tPlayer X: {self.player_x["pawns"][1]}, {self.player_x["pawns"][2]}''')
        print(
            f'''\tPlayer O: {self.player_o["pawns"][1]}, {self.player_o["pawns"][2]}''')

    def print_starting_positions(self):
        print(f'''Starting positions: ''')
        print(
            f'''\tPlayer X: {self.player_x['start_pos']}''')
        print(
            f'''\tPlayer O: {self.player_o['start_pos']} ''')
    # endregion

    # region Input methods
    def select_game_mode(self):
        gm = str.strip(input(
            "Please select a game-mode: PVP - [0] , PVE - [1] or [-1] to Exit\n"))
        while(gm not in ['-1', '0', '1']):
            print('Invalid input!')
            gm = str.strip(input(
                "Please select a game-mode: PVP - [0] , PVE - [1] or [-1] to Exit\n"))
        if(gm == '-1'):
            print("Exiting game...")
            exit(0)
        elif(gm == '0'):
            print("PVP selected")
        elif(gm == '1'):
            print("PVE selected")
        else:
            print("Unexpected input.\nExiting...")
            exit(0)
        return int(gm) == 1

    def select_symbol(self):
        symbol = str.strip(input(
            "Please select your symbol: X - [0] , O - [1] or [-1] to Exit\n"))
        while(symbol not in ['-1', '0', '1']):
            print('Invalid input!')
            symbol = str.strip(input(
                "Please select your symbol: X - [0] , O - [1] or [-1] to Exit\n"))
        if(symbol == '-1'):
            print("Exiting game...")
            exit(0)
        elif(symbol == '0'):
            print("Player has selected X")
            return 'X'
        elif(symbol == "1"):
            print("Player has selected O")
            return 'O'
        else:
            print("Unexpected input.\nExiting...")
            exit(0)

    def input_dimensions(self):
        resp = str.strip(input(
            "Dimensions selection: Recomended (11x14) - [0] , Custom - [1] or [-1] to Exit\n"))
        while(resp not in ['-1', '0', '1']):
            print('Invalid input!')
            resp = str.strip(input(
                "Dimensions selection: Recomended (11x14) - [0] , Custom - [1] or [-1] to Exit\n"))
        if(resp == '-1'):
            print("Exiting game...")
            exit(0)
        if(resp == '0'):
            return (11, 14)

        dims = list(str.strip(input(
            "Input the board dimensions (m,n) seperated by an 'x' (ex. 15x22) or [-1] to Exit\n")).split('x'))
        if('-1' in dims):
            print("Exiting game...")
            exit(0)

        while(len(dims) != 2 or
                not str.isnumeric(dims[0]) or
                not str.isnumeric(dims[1]) or
                not (4 <= int(dims[0]) <= 22) or
                not (4 <= int(dims[1]) <= 28)
              ):
            if(len(dims) != 2):
                print("Invalid input format!")
            elif(not str.isnumeric(dims[0]) or
                 not str.isnumeric(dims[1])):
                print(
                    "Dimensions cant contain letters or other symbols and must be positive integers greater than 4!")
            elif(not (4 <= int(dims[0]) <= 22)):
                print("m must be at most 22 and at least 4!")
            elif(not (4 <= int(dims[1]) <= 28)):
                print("n must be at most 28 and at least 4!")
            else:
                print("Unexpected input")
            dims = list(str.strip(input(
                "Input the board dimensions (m,n) seperated by an 'x' (ex. 15x32) or [-1] to Exit\n")).split('x'))
            if('-1' in dims):
                print("Exiting game...")
                exit(0)
        m, n = map(int, dims)
        print(f'''Board dimensions selected: {m}x{n}''')
        return (m, n)

    def input_number_of_walls(self):

        resp = str.strip(input(
            "Number of walls selection: Recomended (9) - [0] , Custom - [1] or [-1] to Exit\n"))
        while(resp not in ['-1', '0', '1']):
            print('Invalid input!')
            resp = str.strip(input(
                "Number of walls selection: Recomended (9) - [0] , Custom - [1] or [-1] to Exit\n"))
        if(resp == '-1'):
            print("Exiting game...")
            exit(0)
        if(resp == '0'):
            return 9

        k = str.strip(input(
            "Input number of walls (k) or [-1] to Exit\n"))
        # bitan je redosled uslova da int(k) ne bi bacio exception
        while(k != '-1' and not str.isnumeric(k) or not str.isnumeric(k) or int(k) > 18):
            if(not str.isnumeric(k) or int(k) > 18):
                print("k must be a positive integer not bigger than 18")
            k = str.strip(input(
                "Input number of walls (k) or [-1] to Exit\n"))

        if(k == '-1'):
            print("Exiting game...")
            exit(0)
        elif(str.isnumeric(k)):
            print(f'''Number of walls: k={k}''')
        else:
            print("Unexpected input.\nExiting...")
            exit(0)
        return int(k)

    def input_starting_positions(self, player_symbol) -> tuple:
        taken = set()
        if(player_symbol == "O"):
            taken = {*self.player_x['start_pos']}

        recomended = ((4, 4), (self.m-3, 4)
                      ) if player_symbol == "X" else ((4, self.n-3), (self.m-3, self.n-3))

        # Mode selection
        resp = str.strip(input(
            f'''Starting positions selection for {player_symbol}: Recomended {recomended} - [0] , Custom - [1] or [-1] to Exit\n'''))

        intersection = taken.intersection(recomended)

        while(resp not in ['-1', '0', '1'] or resp == "0" and intersection):
            if(resp not in ['-1', '0', '1']):
                print('Invalid input!')
            elif(resp == "0" and intersection):
                print(f'''Fields {intersection} are already taken!''')
            resp = str.strip(input(
                f'''Starting positions selection for {player_symbol}: Recomended {recomended} - [0] , Custom - [1] or [-1] to Exit\n'''))
        if(resp == '-1'):
            print("Exiting game...")
            exit(0)
        if(resp == '0'):
            return recomended

        # field input
        positions = []
        for i in range(1, 3):
            coords = list(str.strip(input(
                f'''Input starting position {i} for player {player_symbol} (row,col) seperated by a comma (ex. 3,6) or [-1] to Exit\n''')).split(','))
            if('-1' in coords):
                print("Exiting game...")
                exit(0)

            while(len(coords) != 2 or
                    not str.isnumeric(coords[0]) or
                    not str.isnumeric(coords[1]) or
                    int(coords[0]) > self.m or
                    int(coords[1]) > self.n or
                    '0' in coords or
                    tuple(map(int, coords)) in taken
                  ):
                if(len(coords) != 2):
                    print("Invalid input format!")
                elif(not str.isnumeric(coords[0]) or
                     not str.isnumeric(coords[1])):
                    print(
                        "Dimensions cant contain letters or other symbols and must be non-zero positive integers!")
                elif(int(coords[0]) > self.m):
                    print(f'''column cant be greater than {self.m}!''')
                elif(int(coords[1]) > self.n):
                    print(f'''row cant be greater than {self.n}!''')
                elif('0' in coords):
                    print(
                        '0 is not a valid coordinate!')
                elif(tuple(map(int, coords)) in taken):
                    print(
                        f'''{tuple(map(int,coords))} is already taken!''')
                else:
                    print("Unexpected input")
                    exit(0)
                coords = list(str.strip(input(
                    f'''Input starting position {i} for player {player_symbol} (row,col) seperated by a comma (ex. 3,6) or [-1] to Exit\n''')).split(','))
                if('-1' in coords):
                    print("Exiting game...")
                    exit(0)
            print(
                f'''Starting position {i} for player {player_symbol}: {tuple(map(int,coords))}''')
            positions.append(tuple(map(int, coords)))
            taken.add(tuple(map(int, coords)))
        return positions
    # endregion

    def generate_wall_placements(self) -> set:
        return {(i, j) for i in range(1, self.m) for j in range(1, self.n)}

    def is_field_blocked(self, indices, top=False, right=False, bottom=False, left=False):
        result = True
        (i, j) = indices
        if not (top or right or bottom or left):
            return False
        if(top):
            result = result and ((i-1, j) in self.h_walls or (
                i-1, j-1) in self.h_walls)
        if(right):
            result = result and ((i-1, j) in self.v_walls or (
                i, j) in self.v_walls)
        if(bottom):
            result = result and ((i, j) in self.h_walls or (
                i, j-1) in self.h_walls)
        if(left):
            result = result and ((i, j-1) in self.v_walls or (
                i-1, j-1) in self.v_walls)
        return result

    def is_valid_wall_placement(self, field, is_vertical):
        global possible_wall_states
        blocked = False
        if(is_vertical):
            if (field in self.possible_v_walls):
                if self.is_wall_connected(field, is_vertical):
                    self.place_wall(None, field, is_vertical,
                                    update_walls=False)
                    if self.zobrist_hash in possible_wall_states:
                        blocked = False
                    else:
                        blocked = self.start_fields_blocked()
                    self.place_wall(None, field, is_vertical,
                                    removal=True, update_walls=False)
            else:
                return False
        else:
            if (field in self.possible_h_walls):
                if self.is_wall_connected(field, is_vertical):
                    self.place_wall(None, field, is_vertical,
                                    update_walls=False)
                    if self.zobrist_hash in possible_wall_states:
                        blocked = False
                    else:
                        blocked = self.start_fields_blocked()
                    self.place_wall(None, field, is_vertical,
                                    removal=True, update_walls=False)
            else:
                return False
        return not blocked

    def move_pawn(self, player, pawn, position):
        old_position = player['pawns'][pawn]

        self.zobrist_hash ^= self.zobrist.zobrist_table[old_position][player['symbol']]
        self.zobrist_hash ^= self.zobrist.zobrist_table[position][player['symbol']]
        self.zobrist_hash ^= self.zobrist.x_turn_to_play_hash

        player['pawns'][pawn] = position
        opponent = self.player_o if player['symbol'] == 'X' else self.player_x

        if position == opponent['pawns'][1]:
            opponent['pawns'][1] = None
        elif position == opponent['pawns'][2]:
            opponent['pawns'][2] = None

    def place_wall(self, player, position, is_vertical, removal=False, update_walls=True):
        if(is_vertical):
            if removal:
                self.v_walls.discard(position)
            else:
                self.v_walls.add(position)
            if update_walls:
                player['v_walls_available'] -= 1
            self.zobrist_hash ^= self.zobrist.zobrist_table[position]['V']
        else:
            if removal:
                self.h_walls.discard(position)
            else:
                self.h_walls.add(position)
            if update_walls:
                player['h_walls_available'] -= 1
            self.zobrist_hash ^= self.zobrist.zobrist_table[position]['H']
        if update_walls:
            self.update_possible_walls_on_placement(position, is_vertical)

    def get_play_move_prompt_and_pattern(self):
        player = self.player_x if self.turn_to_play == 'X' else self.player_o
        prompt = '''Input field in format '[X/O] [1/2] row col '''
        regex_pattern = "^((\s)*[xXoO](\s)+(1|2)(\s)+[1-9a-zA-Z](\s)+[1-9a-zA-Z](\s)"

        if player['v_walls_available'] > 0 and player['h_walls_available'] > 0:
            prompt += '[V/H] row col'
            regex_pattern += "+[vVhH](\s)+[1-9a-zA-Z](\s)+[1-9a-zA-Z](\s)*"

        elif player['v_walls_available'] > 0:
            prompt += 'V row col'
            regex_pattern += "+[vV](\s)+[1-9a-zA-Z](\s)+[1-9a-zA-Z](\s)*"

        elif player['h_walls_available'] > 0:
            prompt += 'H row col'
            regex_pattern += "+[hH](\s)+[1-9a-zA-Z](\s)+[1-9a-zA-Z](\s)*"
        else:
            regex_pattern += "*"
        regex_pattern += ')$'
        prompt += '\n'
        return prompt, regex_pattern

    def play_move(self):
        print(f'''\n\nPlayer {self.turn_to_play}'s turn.''')
        #global transposition_table

        if self.is_pve and self.turn_to_play == self.ai_symbol:
            new_state, new_value, new_pawn_number, new_valid_move, new_wall_pos, new_wall_type = self.iterative_deepening()
            self.apply_state(new_state)
            if self.zobrist_hash in transposition_table:
                transposition_table[self.zobrist_hash]['visited'] = True
            print(
                f'''Player {self.ai_symbol}:\nPawn {new_pawn_number} moved to {tuple(map(self.int_to_field_num,new_valid_move))}''')
            if(new_wall_pos):
                print(
                    f'''{'V' if new_wall_type else 'H'} wall at {tuple(map(self.int_to_field_num,new_wall_pos))}''')

            # transposition_table = dict(filter(lambda kv: kv[1]['walls_placed'] < (
            #     len(new_state.v_walls)+len(new_state.h_walls)), transposition_table.items()))

        else:
            prompt, pattern = self.get_play_move_prompt_and_pattern()
            move = input(prompt)

            if(not re.match(pattern, move)):
                print("Invalid input!")
                return False

            move = move.split()
            symbol = move[0].upper()

            if symbol != self.turn_to_play:
                print(f'''Not {symbol}'s  turn to play.''')
                return False

            player = self.player_x if symbol == "X" else self.player_o
            pawn_number = int(move[1])
            new_field = (self.field_num_to_int(
                move[2]), self.field_num_to_int(move[3]))

            has_walls = len(move) > 4

            is_vertical = move[4].upper() == "V" if has_walls else None
            wall_pos = (self.field_num_to_int(move[5]), self.field_num_to_int(
                move[6])) if has_walls else None

            if(new_field not in self.get_neighbours(symbol, player['pawns'][pawn_number])):
                print('Invalid move!')
                return False

            if(has_walls and not self.is_valid_wall_placement(wall_pos, is_vertical)):
                print("Invalid wall!")
                return False

            self.move_pawn(player, pawn_number, new_field)
            print(
                f'''Player {symbol} moved pawn {pawn_number} to {tuple(map(self.int_to_field_num,new_field))}''')
            if has_walls:
                self.place_wall(player, wall_pos, is_vertical)
                print(
                    f'''{'V' if is_vertical else 'H'} wall at {tuple(map(self.int_to_field_num,wall_pos))}\n\n''')
            self.turn_to_play = 'O' if self.turn_to_play == 'X' else 'X'
        return True

    def winner(self):
        if (self.player_x['pawns'][1] in self.player_o['start_pos'] or
                self.player_x['pawns'][2] in self.player_o['start_pos']):
            return "X"
        elif (self.player_o['pawns'][1] in self.player_x['start_pos'] or
              self.player_o['pawns'][2] in self.player_x['start_pos']):
            return "O"
        else:
            return ""

    def game_loop(self):
        self.print_table()
        # self.print_positions()
        while(not self.is_finished()):
            if(self.play_move()):
                self.print_table()
                # self.print_positions()
        winner = self.winner()
        print(f'''Congratulations! Player {winner} has won the game!''')

    def play_move_in_new_state(self, player_symbol, pawn_number, new_position, wall_placement=None, is_vertical=False):
        new_state = copy.copy(self)
        new_state_player = new_state.player_x if player_symbol == 'X' else new_state.player_o
        new_state.move_pawn(new_state_player, pawn_number, new_position)
        new_state.turn_to_play = 'O' if player_symbol == 'X' else 'X'
        if(wall_placement is not None):
            new_state.place_wall(
                new_state_player, wall_placement, is_vertical)
        return new_state

    def is_wall_within_radius(self, wall_pos, r):
        #opponent = self.player_x if self.turn_to_play == 'O' else self.player_o
        # only opponent radius
        # return (self.manhattan_distance(wall_pos, opponent['pawns'][1]) <= config.wall_distance_radius or
        #         self.manhattan_distance(wall_pos, opponent['pawns'][2]) <= config.wall_distance_radius)
        return (self.manhattan_distance(wall_pos, self.player_x['pawns'][1]) <= r or
                self.manhattan_distance(wall_pos,  self.player_x['pawns'][2]) <= r or
                self.manhattan_distance(wall_pos,  self.player_o['pawns'][1]) <= r or
                self.manhattan_distance(wall_pos,  self.player_o['pawns'][2]) <= r)

    def generate_new_states(self):
        player = self.player_x if self.turn_to_play == 'X' else self.player_o

        v_walls_within_radius = {
            *filter(lambda v: self.is_wall_within_radius(v, config.wall_distance_radius), self.possible_v_walls)}
        h_walls_within_radius = {
            *filter(lambda h: self.is_wall_within_radius(h, config.wall_distance_radius), self.possible_h_walls)}

        increment = 1
        while not v_walls_within_radius and not h_walls_within_radius and player['v_walls_available'] > 0 and player['h_walls_available'] > 0:
            v_walls_within_radius = {
                *filter(lambda v: self.is_wall_within_radius(v, config.wall_distance_radius+increment), self.possible_v_walls)}
            h_walls_within_radius = {
                *filter(lambda h: self.is_wall_within_radius(h, config.wall_distance_radius+increment), self.possible_h_walls)}
            increment += 1

        non_blocking_v_walls = {} if not (player['v_walls_available'] > 0 and v_walls_within_radius) else {*
                                                                                                           filter(lambda v: self.is_valid_wall_placement(v, is_vertical=True), v_walls_within_radius)}
        non_blocking_h_walls = {} if not (player['h_walls_available'] > 0 and h_walls_within_radius) else {*
                                                                                                           filter(lambda h: self.is_valid_wall_placement(h, is_vertical=False), h_walls_within_radius)}

        for pawn_number in [1, 2]:
            pawn_pos = player['pawns'][pawn_number]
            neighbours = self.get_neighbours(self.turn_to_play, pawn_pos)
            for valid_move in neighbours:
                for v_wall_placement in non_blocking_v_walls:
                    yield self.play_move_in_new_state(self.turn_to_play, pawn_number, valid_move, v_wall_placement, is_vertical=True), pawn_number, valid_move, v_wall_placement, True
                for h_wall_placement in non_blocking_h_walls:
                    yield self.play_move_in_new_state(self.turn_to_play, pawn_number, valid_move, h_wall_placement, is_vertical=False), pawn_number, valid_move, h_wall_placement, False
                if (not non_blocking_v_walls and not non_blocking_h_walls):
                    yield self.play_move_in_new_state(self.turn_to_play, pawn_number, valid_move), pawn_number, valid_move, None, None

    def manhattan_distance(self, start, end, factor=1):
        return (abs(end[0]-start[0])+abs(end[1]-start[1]))*factor

    def start_fields_blocked(self):
        # if the state was evaluated previously that means that it is valid
        if (self.zobrist_hash in possible_wall_states):
            return False

        if not self.exists_path(
                'O', 1, {*self.player_x['start_pos'], self.player_o['pawns'][2]}):
            return True
        if not self.exists_path(
                'X', 1, {*self.player_o['start_pos'], self.player_x['pawns'][2]}):
            return True

        return False

    def iterative_deepening(self):
        best_state, best_value = None, float('-inf')
        start_time = timeit.default_timer()
        best_pawn_number, best_valid_move, best_wall_pos, best_wall_type = None, None, None, None
        for i in range(1, 100):
            transposition_table.clear()
            new_state, new_value, new_pawn_number, new_valid_move, new_wall_pos, new_wall_type = self.alphabeta(self, 0, i, float(
                '-inf'), float('inf'), start_time, maximizing_player=True)

            best_state, best_value, best_pawn_number, best_valid_move, best_wall_pos, best_wall_type = (
                new_state, new_value, new_pawn_number, new_valid_move, new_wall_pos, new_wall_type) if (not best_state and timeit.default_timer()-start_time >= config.max_sc_per_move) or (new_value > best_value and timeit.default_timer()-start_time < config.max_sc_per_move) else (
                    best_state, best_value, best_pawn_number, best_valid_move, best_wall_pos, best_wall_type)

            if timeit.default_timer()-start_time >= config.max_sc_per_move:
                break
        print(f'''Time taken:\t{timeit.default_timer()-start_time}s''')
        return best_state, best_value, best_pawn_number, best_valid_move, best_wall_pos, best_wall_type

    def alphabeta(self, state, depth, max_depth, alpha, beta, start_time=None, maximizing_player=True):
        global transposition_table

        # if depth > 0 and state.zobrist_hash in transposition_table:
        #     return state, transposition_table[state.zobrist_hash]['value']

        if depth == max_depth or state.is_finished() or (start_time and timeit.default_timer()-start_time >= config.max_sc_per_move):
            # if state.zobrist_hash not in transposition_table:
            state.store_state(state.evaluate_state(), depth)
            return state, transposition_table[state.zobrist_hash]['value'], None, None, None, None

        new_states = state.generate_new_states()
        best_value = float('-inf') if maximizing_player else float('inf')
        best_state = state
        best_pawn_number, best_valid_move, best_wall_pos, best_wall_type = None, None, None, None
        for child_state, pawn_number, valid_move, wall_pos, wall_type in new_states:
            new_state, new_value, _, _, _, _ = state.alphabeta(
                child_state, depth + 1, max_depth, alpha, beta, start_time, not maximizing_player)

            if maximizing_player:
                best_state, best_value, best_pawn_number, best_valid_move, best_wall_pos, best_wall_type = (
                    child_state, new_value, pawn_number, valid_move, wall_pos, wall_type) if new_value > best_value else (best_state, best_value, best_pawn_number, best_valid_move, best_wall_pos, best_wall_type)
                if best_value >= beta:
                    break
                alpha = max(alpha, best_value)
            else:
                best_state, best_value, best_pawn_number, best_valid_move, best_wall_pos, best_wall_type = (
                    child_state, new_value, pawn_number, valid_move, wall_pos, wall_type) if new_value < best_value else (best_state, best_value, best_pawn_number, best_valid_move, best_wall_pos, best_wall_type)
                if best_value <= beta:
                    break
                beta = min(beta, best_value)

            if start_time and timeit.default_timer()-start_time >= config.max_sc_per_move:
                break
        state.store_state(best_value, depth)
        return best_state, best_value, best_pawn_number, best_valid_move, best_wall_pos, best_wall_type

    def evaluate_state(self):
        if self.is_finished():
            if self.winner() == self.ai_symbol:
                return 1000000
            else:
                return -1000000

        maximizing, minimizing = (self.player_x, self.player_o) if self.ai_symbol == 'X' else (
            self.player_o, self.player_x)

        raw_manhattan_maximizing = min(map(lambda pawn: self.manhattan_distance(pawn, min(
            minimizing['start_pos'], key=lambda start: self.manhattan_distance(pawn, start))), maximizing['pawns'].values()))
        raw_manhattan_minimizing = min(map(lambda pawn: self.manhattan_distance(pawn, min(
            maximizing['start_pos'], key=lambda start: self.manhattan_distance(pawn, start))), minimizing['pawns'].values()))

        maximizing_dists = [self.min_distance(
            maximizing, y, minimizing['start_pos']) for y in {1, 2}]
        minimizing_dists = [self.min_distance(
            minimizing, y, maximizing['start_pos']) for y in {1, 2}]

        maximizing_dists = (maximizing_dists[0], maximizing_dists[1]) if maximizing_dists[0] < maximizing_dists[1] else (
            maximizing_dists[1], maximizing_dists[0])
        minimizing_dists = (minimizing_dists[0], minimizing_dists[1]) if minimizing_dists[
            0] < minimizing_dists[1] else (minimizing_dists[1], minimizing_dists[0])

        max_path = 30

        score = 1000*(minimizing_dists[0] - maximizing_dists[0])/max_path

        if (maximizing['v_walls_available']+maximizing['h_walls_available']) == 0 and maximizing_dists[0] > minimizing_dists[0]:
            return -1000000
        if (minimizing['v_walls_available']+minimizing['h_walls_available']) == 0 and maximizing_dists[0] < minimizing_dists[0]:
            return 1000000

        if self.zobrist_hash in transposition_table and transposition_table[self.zobrist_hash]['played']:
            score -= 0.1
        if maximizing_dists[0] == minimizing_dists[0]:
            score += 10*(minimizing_dists[1] - maximizing_dists[1])/max_path

        score += 2*(raw_manhattan_minimizing -
                    raw_manhattan_maximizing)/max_path

        #score += 1*(sum(minimizing_dists) - sum(maximizing_dists))/max_path
        # min_sp_max = maximizing_dists[0] - raw_manhattan_maximizing
        # min_sp_min = minimizing_dists[0] - raw_manhattan_minimizing

        # score += 2*(min_sp_min - min_sp_max)/max_path
        # min_sp_max = maximizing_dists[1] - raw_manhattan_maximizing
        # min_sp_min = minimizing_dists[1] - raw_manhattan_minimizing
        # score += 1*(min_sp_min - min_sp_max)/max_path

        # if self.turn_to_play == self.ai_symbol:
        #     score += 0.01
        # else:
        #     score -= 0.01
        return score*10000

    def apply_state(self, state):
        self.is_pve = state.is_pve
        self.player_symbol = state.player_symbol
        self.ai_symbol = state.ai_symbol

        self.m, self.n = state.m, state.n

        self.v_walls = {*state.v_walls}
        self.h_walls = {*state.h_walls}

        self.turn_to_play = state.turn_to_play

        self.player_x = {**state.player_x}
        self.player_x['pawns'] = {**state.player_x['pawns']}

        self.player_o = {**state.player_o}
        self.player_o['pawns'] = {**state.player_o['pawns']}

        self.possible_v_walls = {*state.possible_v_walls}
        self.possible_h_walls = {*state.possible_h_walls}

        self.zobrist = state.zobrist
        self.zobrist_hash = state.zobrist_hash

    def __eq__(self, state):
        return (
            {*self.player_x['pawns'].values()} == {*state.player_x['pawns'].values()} and
            {*self.player_o['pawns'].values()} == {*state.player_o['pawns'].values()} and
            len(self.h_walls) == len(state.h_walls) and self.h_walls == state.h_walls and
            len(self.v_walls) == len(
                state.v_walls) and self.v_walls == state.v_walls and
            self.turn_to_play == state.turn_to_play
        )

    def get_game_hash_name(self):
        return 'transposition_tables/'+str(self.zobrist.tp_name_hash(self))+'.json'

    def store_state(self, best_value, depth):
        global transposition_table
        global possible_wall_states
        possible_wall_states.add(self.zobrist_hash)

        if self.zobrist_hash not in transposition_table:
            transposition_table[self.zobrist_hash] = {}

        transposition_table[self.zobrist_hash]['value'] = best_value
        transposition_table[self.zobrist_hash]['depth'] = depth
        transposition_table[self.zobrist_hash]['walls_placed'] = len(
            self.v_walls) + len(self.h_walls)
        transposition_table[self.zobrist_hash]['played'] = False

    def exists_path(self, player_symbol: str, pawn_num: int, end_fields: tuple):

        def remove_current_and_get_new_closest_end(closest_end, ends):
            ends.discard(closest_end)
            if not ends:
                return None
            else:
                return min(ends, key=lambda end: self.manhattan_distance(end, curr_field))
        player = self.player_x if player_symbol == 'X' else self.player_o
        start_field = player['pawns'][pawn_num]
        ends = {*end_fields}

        open_set = {start_field}
        closed_set = set()

        closest_end = min(
            end_fields, key=lambda end: self.manhattan_distance(end, start_field))

        g = {start_field: 0}
        while open_set and ends:
            curr_field = min(
                open_set, key=lambda node: self.manhattan_distance(node, closest_end))

            player['pawns'][pawn_num] = curr_field

            if(curr_field == closest_end):
                closest_end = remove_current_and_get_new_closest_end(
                    closest_end, ends)
                while closest_end in closed_set:
                    closest_end = remove_current_and_get_new_closest_end(
                        closest_end, ends)
                if not closest_end:
                    break

            neighbours = self.get_neighbours(
                player_symbol, curr_field, pathfinding=True)
            for neighbour in neighbours:
                cost = self.manhattan_distance(neighbour, curr_field)

                if(curr_field == closest_end):
                    closest_end = remove_current_and_get_new_closest_end(
                        closest_end, ends)
                    while closest_end in closed_set:
                        closest_end = remove_current_and_get_new_closest_end(
                            closest_end, ends)
                    if not closest_end:
                        player['pawns'][pawn_num] = start_field
                        return True

                if(neighbour not in open_set and neighbour not in closed_set):
                    open_set.add(neighbour)
                    g[neighbour] = g[curr_field] + cost
                else:
                    if g[neighbour] > g[curr_field] + cost:
                        g[neighbour] = g[curr_field] + cost
                        if neighbour in closed_set:
                            closed_set.remove(neighbour)
                            open_set.add(neighbour)
            open_set.remove(curr_field)
            closed_set.add(curr_field)
        player['pawns'][pawn_num] = start_field
        return not ends

    def min_distance(self, player, pawn_number, ends):
        open_set = {*ends}
        closed_set = set()
        g = {start: 0 for start in ends}
        player['pawns'][3] = None
        end = player['pawns'][pawn_number]
        while open_set:
            node = min(
                open_set, key=lambda x: g[x]+self.manhattan_distance(x, end))
            player['pawns'][3] = node
            if node == end:
                break
            neighbours = self.get_neighbours(
                player['symbol'], node, pathfinding=True)
            for neighbour in neighbours:
                cost = self.manhattan_distance(neighbour, node)
                if neighbour == end:
                    player['pawns'].pop(3, None)
                    return g[node] + cost
                if neighbour not in open_set and neighbour not in closed_set:
                    open_set.add(neighbour)
                    g[neighbour] = g[node] + cost
                else:
                    if g[neighbour] > g[node] + cost:
                        g[neighbour] = g[node] + cost
                        if neighbour in closed_set:
                            closed_set.remove(neighbour)
                            open_set.add(neighbour)
            open_set.remove(node)
            closed_set.add(node)
        player['pawns'].pop(3, None)

        return g[end] if end in g else 1000


if __name__ == '__main__':
    game = Blockade()
    game.initialize_default()

    if(os.path.exists(game.get_game_hash_name())):
        transposition_table = load_transposition_table(
            game.get_game_hash_name())

    #start_time = timeit.default_timer()
    game.game_loop()
    #print(timeit.default_timer() - start_time)

    if not os.path.exists('zobrist_data.csv'):
        game.zobrist.save_to_file()
    # save_transposition_table(
    #    transposition_table, game.get_game_hash_name())
