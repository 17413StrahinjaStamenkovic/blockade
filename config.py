is_pve = True
player_symbol = 'O'
ai_symbol = 'X'

m, n = 11, 14
k = 9
player_x_start_pos = ((4, 4), (m-3, 4))
player_o_start_pos = ((4, n-3), (m-3, n-3))

wall_distance_radius = 3
max_sc_per_move = 30
