import random
import csv
import os
from unittest import skip


class Zobrist:
    zobrist_table = {}
    x_turn_to_play_hash = None
    x_ai_symbol_hash = None
    m_hash, n_hash = None, None

    def __init__(self):
        if(os.path.exists('zobrist_data.csv')):
            self.load_from_file()
        else:
            self.zobrist_table = {(i, j): {k: random.randint(1, 2**64 - 1) for k in ['X', 'O', 'V', 'H', 'XS1', 'XS2', 'OS1', 'OS2']}
                                  for i in range(1, 23) for j in range(1, 29)}

            self.x_turn_to_play_hash = random.randint(1, 2**64 - 1)
            self.x_ai_symbol_hash = random.randint(1, 2**64 - 1)
            self.m_hash = random.randint(1, 2**64 - 1)
            self.n_hash = random.randint(1, 2**64 - 1)

    def load_from_file(self):
        with open('zobrist_data.csv') as f:
            i, j = 1, 1
            data = f.read().split('\n\n')

            table_data = data[0].split('\n')
            non_table_data = data[1].split(',')

            self.x_turn_to_play_hash = int(non_table_data[0])
            self.x_ai_symbol_hash = int(non_table_data[1])
            self.m_hash = int(non_table_data[2])
            self.n_hash = int(non_table_data[3])

            for dict_data in table_data:
                dict_data = dict_data.split(',')
                self.zobrist_table[(i, j)] = {
                    'X': int(dict_data[0]),
                    'O': int(dict_data[1]),
                    'V': int(dict_data[2]),
                    'H': int(dict_data[3]),
                    'XS1': int(dict_data[4]),
                    'XS2': int(dict_data[5]),
                    'OS1': int(dict_data[6]),
                    'OS2': int(dict_data[7])
                }
                j += 1
                if j == 29:
                    j = 1
                    i += 1
        return self

    def save_to_file(self):
        with open("zobrist_data.csv", "w", newline='') as f:
            csv_writer = csv.writer(f)
            csv_writer.writerows(
                list(map(lambda d: d.values(), self.zobrist_table.values())))
            csv_writer.writerow([])
            csv_writer.writerow(
                [self.x_turn_to_play_hash, self.x_ai_symbol_hash, self.m_hash, self.n_hash])

    def calculate_hash(self, game):
        h = 0
        if game.turn_to_play == 'X':
            h ^= self.x_turn_to_play_hash
        h ^= self.zobrist_table[game.player_x['pawns'][1]]['X']
        h ^= self.zobrist_table[game.player_x['pawns'][2]]['X']
        h ^= self.zobrist_table[game.player_o['pawns'][1]]['O']
        h ^= self.zobrist_table[game.player_o['pawns'][2]]['O']

        for v_wall in game.v_walls:
            h ^= self.zobrist_table[v_wall]['V']
        for h_wall in game.h_walls:
            h ^= self.zobrist_table[h_wall]['H']

        return h

    def tp_name_hash(self, game):
        h = 0
        if game.ai_symbol == 'X':
            h ^= self.x_ai_symbol_hash
        h ^= self.zobrist_table[game.player_x['start_pos'][0]]['XS1']
        h ^= self.zobrist_table[game.player_x['start_pos'][1]]['XS2']
        h ^= self.zobrist_table[game.player_o['start_pos'][0]]['OS1']
        h ^= self.zobrist_table[game.player_o['start_pos'][1]]['OS2']
        return h
